Modded nick app with:

* Rainbow cycling font color
* Spinning and rotating text, also sensitive to the accelerometer
* Different fonts for name and pronouns
* Plays random notes when petals are touched.
